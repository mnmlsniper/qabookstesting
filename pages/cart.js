const { I } = inject();

module.exports = {
    buttons: {
        searchBtn: '.input-group-btn',
        makeOrder: '//a[contains(text(), "Оформление заказа")]',
        continuePayment: '#button-payment-address',
        continueShipping: '#button-shipping-address',
        continueShipMethod: '#button-shipping-method',
        continuePayMethod: '#button-payment-method',
        confirm: '//input[@name="agree"]/following-sibling::input[@value="Продолжить"]',
        finalConfirm: '[value="Подтверждение заказа"]',
        finish: '.btn-primary',
        clear: '[data-original-title="Удалить"]',
    },
    elements: {
     table: '//form[@enctype="multipart/form-data"]',
     amount: '//strong[contains(text(),"Итого:")]/following::td[@class="text-right"]',
    },
    checkboxes: {
        agree: '[name="agree"]',
    },

    async clearCart() {
        I.amOnPage('/cart');
        const isThereProd = await tryTo(() => I.waitForElement({ xpath: this.elements.table }), 2);
        if (isThereProd) I.click({ css: this.buttons.clear });
        I.wait(1);

    },

    createOrder() {
        I.waitForElement({ xpath: this.elements.table });
        I.click({ xpath: this.buttons.makeOrder });
        I.waitForText('Оформление заказа');
        I.click({ css: this.buttons.continuePayment });
        I.click({ css: this.buttons.continueShipping });
        I.click({ css: this.buttons.continueShipMethod });
        I.click({ css: this.buttons.continuePayMethod });
        I.click({ css: this.checkboxes.agree });
        I.click({ xpath: this.buttons.confirm });
        I.click({ css: this.buttons.finalConfirm });
        I.waitForText('Ваш заказ принят!');
    },

    async getHeadText() {
        I.waitForText('Спасибо за покупки в нашем интернет-магазине!');
        const heading = await I.grabTextFrom({ xpath: '//h1' });
        return heading;
    },
}
