let BuildUser = require ('../data/buildUser');
const assert = require('assert').strict;
const { SUCCSESS_REGISTER } = require('../data/texts');
const { BASE_URL } = require('../data/urls');
const { mainPage, registrationPage } = inject();
const user = BuildUser.getUser();

Feature('Registration');

Before(({ I })=> {
    I.amOnPage(BASE_URL);
});

Scenario('New user registration @feature', async ({ I}) => {
    mainPage.goToRegister();
    registrationPage.enterBasicData(user.name, user.surname, user.email, user.pass)
    registrationPage.enterAddress(user.address, user.city);
    registrationPage.enterPassword(user.pass);
    registrationPage.checkAgreeOption()
    registrationPage.submitRegistration();
    const headText = await registrationPage.getHeadText();
    assert.equal(headText, SUCCSESS_REGISTER);
});
