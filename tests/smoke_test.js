const assert = require('assert').strict;
const { SUCCSESS_LOGIN } = require('../data/texts');
const { BASE_URL } = require('../data/urls');
const { mainPage, productCell } = inject();

Feature('Smoke test');

Before(({ I })=> {
    I.amOnPage(BASE_URL);
});

Scenario('Fast health check @smoke', async ({ I}) => {
    mainPage.smokeTest();
    const prodCheck = await productCell.seeProduct();
    assert.ok(prodCheck, 'На главной странице не загрузился ни один товар.');
});
