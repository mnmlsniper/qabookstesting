const assert = require('assert').strict;
const { SUCCESS_SEARCH, UNSUCCESS_SEARCH } = require('../data/texts');
const { BASE_URL, SEARCH_URL } = require('../data/urls');
const { mainPage, searchPage, productCell } = inject();

Feature('Product search');

Scenario('Search from main page @feature', async ({ I}) => {
    I.amOnPage(BASE_URL);
    const sought = 'путь';
    mainPage.goToSearch(sought);
    const headText = await searchPage.getHeadText();
    assert.equal(headText, `${SUCCESS_SEARCH}${sought}`);
});

Scenario('Success search @feature', async ({ I}) => {
    I.amOnPage(SEARCH_URL);
    const sought = 'кухня';
    searchPage.enterKeyword(sought);
    searchPage.submitSearch();
    const headText = await searchPage.getHeadText();
    assert.equal(headText, `${SUCCESS_SEARCH}${sought}`);
    const title = await productCell.getProductTitle();
    assert.equal(title.includes(sought), true);
});

Scenario('Unsuccessful search @feature', async ({ I}) => {
    I.amOnPage(SEARCH_URL);
    const sought = 'печать';
    searchPage.enterKeyword(sought);
    searchPage.submitSearch();
    const headText = await searchPage.getHeadText();
    assert.equal(headText, `${SUCCESS_SEARCH}${sought}`);
    I.waitForText(UNSUCCESS_SEARCH);
});